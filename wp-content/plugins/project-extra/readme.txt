=== HRB Extend ===
Contributors: webkod3r
Donate link: http://webkod3r.es/
Tags: hrb, adjustments
Requires at least: 3.0.0
Tested up to: 4.4.2
Stable tag: 1.0.0
License: MIT
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Este plugin fue hecho simplemente con el objetivo de agregar los ajustes al sitio, sin la necesidad de estar modificando
el codigo original del tema.

== Description ==

Este plugin fue hecho simplemente con el objetivo de agregar los ajustes al sitio, sin la necesidad de estar modificando
el codigo original del tema.

== Installation ==

El proceso de instalacion es bastante simple. Solo siga los siguientes pasos:

1. Suba la carpeta 'qie-adjustments' al directorio '/wp-content/plugins/'
2. Active el plugin a través de la sección 'Plugins' en el panel de adnimistración de su sitio en Wordpress
3. Listo!!! el plugin hará el resto del trabajo :)

== Frequently Asked Questions ==

= Sin preguntas? =

Muy sencillo cierto ;)!!!

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets 
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png` 
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 1.0.1 =
* Mejorando la ubicación del selector de monedas en el top_menu

= 1.0.0 =
* Version inicial