<?php
// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'tripolis');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'password');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '*amcZ_.3}R-2w`Y?03~z=S5K-*ImqsriZ.RZ^+L9pQIR`j<^N+TBi.=&mU6j:t`h');
define('SECURE_AUTH_KEY', '[h/ajX6wD<STFkcSNwev3oY4q^dQx5;NF1_[*6Mz]xn,,k-4Mo~B<r6a(h9rt8 d');
define('LOGGED_IN_KEY', 'c|_2#pCn<&GC j3CtdR^bm=.2#Ai6zm7eRsE:c=~ Aq6Uk+!+PTLE{d)>9DIhZpQ');
define('NONCE_KEY', 'w<iahh)o{e_Al>Oo`WanHB<{v7ne2I.Y*XW=J>qqKO,6d0/f2?*PDpzpjFfohwqe');
define('AUTH_SALT', 'X%eD#R~z~dmdoabogJl`54ze=}]1bOXLQ,jWevL[s*cy55IA</>)Y?c_h1(|cCMy');
define('SECURE_AUTH_SALT', 'wm<AKS+jvf:a[qNvGxI 3Ol]kwD7;>`n;#r3!sTd:_A%V@I;^n]}Xo4=<99Lv:y*');
define('LOGGED_IN_SALT', 'h^}z2rfy},or?^+57C7^[/yE5@v3?BzS15_?t_>,sf;i/%f8AOpVObDSdDcbqO0+');
define('NONCE_SALT', 'l@Jc@U3R9NJK]{Yva: 0yRrl2]6b@;GY;CiG2gS$r[/l~{T9=<*R []c*6o~1-hk');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* Constantes introducidas. Elimina las revisiones de los posts y al autoguardado */
define('AUTOSAVE_INTERVAL', 300); // seconds
define('WP_POST_REVISIONS', false);
define('WP_MEMORY_LIMIT', '256M');

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

