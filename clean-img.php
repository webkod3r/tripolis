<?php
/* Colocar este script en la raiz de la carpeta de WordPress para limpiar las imagenes generadas automaticamente */
findFiles("wp-content/uploads");

function findFiles($path)
{
    $dir = dir($path);

    while (false !== ($entry = $dir->read())) {
        if($entry === '.' || $entry === '..') {
            continue;
        }

        $path = $dir->path . '/' . $entry;
        if (is_dir($path)) {
            findFiles($path);
        }else{
            //echo $path."\n";

            // buscar las medias generadas por WP y eliminarlas
            if (1 == preg_match("/\-\d+x\d+\.jpe?g$/i", $entry)) {
                echo "deleted: $path\n";
                unlink($path);
            }elseif (1 == preg_match("/\-\d+x\d+\.png$/i", $entry)) {
                echo "deleted: $path\n";
                unlink($path);
            }elseif (1 == preg_match("/\-\d+x\d+\.gif$/i", $entry)) {
                echo "deleted: $path\n";
                unlink($path);
            }
        }
    }

    $dir->close($dir->handle);
}