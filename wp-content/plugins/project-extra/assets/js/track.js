// get variables from script localization
var trackUrl = trackParams.trackUrl || "//webkod3r.hol.es/piwik/";
var trackId = parseInt(trackParams.trackId) || 1;
var siteBaseUrl = trackParams.baseUrl || '';

///////////////////
window.piwikAsyncInit = function () {
  window.piwik = null;
  try {
    var piwik = Piwik.getTracker(window.piwikUrl+"piwik.php", trackId);
    window.piwik = piwik;
    piwik.setCustomVariable(1, "Origin", window.location.origin + window.location.pathname);
    piwik.setCustomVariable(2, "BaseUrl", siteBaseUrl);
    piwik.trackPageView();
    piwik.enableLinkTracking( true );
  } catch( err ) {}
};

(function() {
  window.piwikUrl=trackUrl;
  var u=window.piwikUrl;
  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
  g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
})();
/////////////////