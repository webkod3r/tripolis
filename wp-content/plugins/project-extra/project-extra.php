<?php
/*
Plugin Name: Project Extra
Plugin URI: http://webkod3r.es/wp-plugins/project-extra
Description: Este plugin es realizado con el propósito de modificar via plugin los ajustes necesarios para la personalización del proyecto.
Version: 1.0.0
Author: Pablo Molina
Author URI: http://webkod3r.es
License: MIT
*/
define('PEXTRA_VERSION', '1.0.0');
define('PEXTRA_PLUGIN_DIR', realpath(dirname(__FILE__)));
define('PEXTRA_PLUGIN_SLUG', basename(PEXTRA_PLUGIN_DIR));
define('PEXTRA_URL', plugin_dir_url(__FILE__));
define('PEXTRA_ASSETS', PEXTRA_URL . 'assets/');
define('PEXTRA_TXTD', 'pextra');

/**
 * Agregar funcciones prioritarias en el init 
 */
add_action('init', 'pextra_init');
function pextra_init()
{
	// wp_enqueue_style( 'custom-register-form', PEXTRA_ASSETS . '/css/custom-register-form.css', array(), PEXTRA_VERSION );
	//wp_enqueue_script('custom-main', PEXTRA_ASSETS . '/js/main-extended.js', array('jquery'), PEXTRA_VERSION, true );
	/*wp_localize_script('custom-main', 'scriptParams', array(
		'baseUrl' => get_option('siteurl'),
	));*/
	//wp_enqueue_script('services-trigger', PEXTRA_ASSETS . '/js/services-trigger.js', array('jquery'), PEXTRA_VERSION, true);

	// Por defecto incluir script para seguimiento
	wp_enqueue_script('custom-track', PEXTRA_ASSETS . '/js/track.js', array('jquery'), PEXTRA_VERSION, true);
	wp_localize_script('custom-track', 'trackParams', array(
		'baseUrl' => get_option('siteurl'),
		'trackUrl' => '//webkod3r.hol.es/piwik/',
		'trackId' => 7,
	));
}

// Register custom jQuery script
add_action('wp_enqueue_scripts', 'pextra_register_jquery');
function pextra_register_jquery() {
	if (!is_admin()) {
		wp_deregister_script('jquery-core');
		wp_register_script('jquery-core', PEXTRA_ASSETS . '/vendor/jquery/jquery.min.js', true, '1.11.3');
		wp_enqueue_script('jquery-core');

		//wp_deregister_script('jquery-migrate');
		//wp_register_script('jquery-migrate', 'http://cdn.yourdomain.com/wp-includes/js/jquery/jquery-migrate.min.js', true, '1.2.1');
		//wp_enqueue_script('jquery-migrate');
	}
}

//-----------------------------------------------------------------------------
// Cargar los textos en dependencia del idioma
//-----------------------------------------------------------------------------
add_action('init', 'pextra_load_plugin_textdomain');
function pextra_load_plugin_textdomain()
{
	$locale = apply_filters('plugin_locale', get_locale(), PEXTRA_TXTD);

	// Global + Frontend Locale
	load_textdomain(PEXTRA_TXTD, WP_LANG_DIR . sprintf('/%1$s/%2$s.mo', PEXTRA_PLUGIN_SLUG, $locale));
	load_plugin_textdomain(PEXTRA_TXTD, false, plugin_basename(dirname(__FILE__)) . "/languages");
}

//-----------------------------------------------------------------------------
// Incluir helpers
//-----------------------------------------------------------------------------
include_once PEXTRA_PLUGIN_DIR . '/includes/helpers/duplicate-post.php';

// uncomment if you need extra image sizes and edit script
//include_once PEXTRA_PLUGIN_DIR . '/includes/helpers/images-sizes.php';

// uncomment if you need add extra currency and edit script
include_once PEXTRA_PLUGIN_DIR . '/includes/helpers/custom-currency.php';

//include_once dirname(__FILE__) . '/includes/helpers/currencies-in-menu.php';
//include_once dirname(__FILE__) . '/includes/helpers/extra-customer-fields.php';
