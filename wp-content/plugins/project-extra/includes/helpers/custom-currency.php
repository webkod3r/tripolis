<?php
/*
 * This helper is used if you need extra currency in your site
 */

global $newCurrency;
$newCurrency = array(
	'name' => __('Perú Nuevo Sol', PEXTRA_TXTD),
	'iso_code' => 'PEN',
	'symbol' => 'S/.',
);

add_filter('woocommerce_currencies', 'pextra_custom_currency');
function pextra_custom_currency($currencies) {
	global $newCurrency;

	$currencies[$newCurrency['iso_code']] = $newCurrency['name'];
	return $currencies;
}

add_filter('woocommerce_currency_symbol', 'pextra_custom_currency_symbol', 10, 2);
function pextra_custom_currency_symbol($currency_symbol, $currency) {
	global $newCurrency;

	if ($currency == $newCurrency['iso_code']) {
		$currency_symbol = $newCurrency['symbol'];
	}

	return $currency_symbol;
}